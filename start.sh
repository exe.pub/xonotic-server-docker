#!/bin/sh

set -x

DARKPLACES=./darkplaces/darkplaces-dedicated

ARGS="-xonotic -userdir /xonotic/userdir -game data_static -game data_generated -game data"

export LD_LIBRARY_PATH=/xonotic/game/d0_blind_id/.libs

if [ -z "$PORT" ]
then
   PORT=26000
fi

if [ -z "$RCON_PASSWORD" ]
then 
$DARKPLACES $ARGS
else
$DARKPLACES $ARGS +port $PORT +rcon_password $RCON_PASSWORD
fi
