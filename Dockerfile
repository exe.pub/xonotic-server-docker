FROM ubuntu:18.04 as buildfarm
ARG VER=unknown
LABEL version=$VER

WORKDIR /

RUN apt-get update && apt-get -y install \
    autoconf \
    build-essential \
    curl \
    git-core \
    libcurl4 \
    libcurl4-openssl-dev \
    libtool \
    libgmp-dev \
    libjpeg-turbo8-dev \
    libsdl2-dev \
    libxpm-dev \
    p7zip-full \
    rsync \
    sed \
    xserver-xorg-dev \
    unzip \
    zlib1g-dev

COPY . /xonotic
WORKDIR /xonotic

RUN touch data/xonotic-music.pk3dir.no
RUN touch data/xonotic-maps.pk3dir.no
RUN touch netradiant.no
RUN ./all clean
RUN ./all compile -r -0 dedicated

RUN mkdir /pk3s
RUN mkdir /tmp/build-data



WORKDIR /xonotic/data/xonotic-data.pk3dir
RUN mv qcsrc/csprogs-$VER.pk3 /pk3s

RUN rsync -rv --exclude=.git --exclude=.tmp --exclude=textures --exclude=*.tga \
     --exclude=*.jpg --exclude=*.map --exclude=*.qh --exclude=*.qc \
     --exclude=*.ogg --exclude=*.wav --exclude=*.shader --exclude=*.mp3  \
     --exclude=*.inc --exclude=*.ase --exclude=.gitignore --exclude=.gitattributes \
     --exclude=*.yml --exclude=.tx --exclude=Makefile --exclude=*.sp2 --exclude=*.mid \
     --exclude=README.md --exclude=cmake --exclude=CMakeLists.txt --exclude=*.sh  \
     --exclude=Dockerfile --exclude=docker --exclude=*.pk3 \
     --exclude=*.sh --exclude=*.dem "" /tmp/build-data

WORKDIR /tmp/build-data

RUN 7za a -tzip -mx=9  "/pk3s/xonotic-data-$VER.pk3"  ./


FROM ubuntu:18.04
ARG VER=unknown
LABEL version=$VER

RUN apt-get update && apt-get -y install \
  curl \
  libcurl4 \
  libcurl4-openssl-dev \
  libgmp10 \
  libjpeg-turbo8 \
  libxpm4 \
  zlib1g

RUN useradd -m -d /xonotic/ xonotic 
RUN mkdir -p /xonotic/game /xonotic/userdir
RUN mkdir -p /xonotic/game/darkplaces/
RUN mkdir -p /xonotic/game/data/
RUN mkdir -p /xonotic/game/d0_blind_id/
COPY start.sh /xonotic/game/
COPY --from=buildfarm /xonotic/d0_blind_id/ /xonotic/game/d0_blind_id/
COPY --from=buildfarm /xonotic/key_*.d0pk /xonotic/game/
COPY --from=buildfarm /xonotic/darkplaces/darkplaces-dedicated /xonotic/game/darkplaces/
COPY --from=buildfarm /pk3s/* /xonotic/game/data/

ENV LD_LIBRARY_PATH /xonotic/game/d0_blind_id/.libs
VOLUME /xonotic/userdir
WORKDIR /xonotic/game

EXPOSE 26000/udp

ENTRYPOINT ["/usr/bin/nice", "-n", "-15", "/bin/sh", "./start.sh"]
